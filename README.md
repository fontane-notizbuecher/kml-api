# KML API for tei:place

This package provides a RESTful API to generate KML files from `tei:place`.

It is created for the [eXist](https://exist-db.org) database and it is an
extension to the SADE package created for [TextGrid](https://textgrid.de) (a
very complete infrastructure for the text-based humanities).

## What it does
This package provides a RESTful API to KML documents converted from `tei:place`.

### REST Path
+ https://[YOUR_INSTANCE]/exist/apps/kml/{notebooks}/fontane.kml

### Parameters
#### notebooks
A URL encoded Regular Expression to query the TextGrid metadata for a document
title.

The Regular Expression will run against a title. For all matches the internal
URIs are collected (this is the TextGrid specific part). These URIs are
evaluated against their mentions within
`//tei:place/tei:linkGrp/tei:link/@target`.

## Dependency
Its dependency for [SADE](expath-pkg.xml#L4) is just for a specific deployment
scenario and can be removed in any case where you not want to use SADE. But in
general you will have to do some customizations, as it includes some queries
specific for TextGrid.

## Build
`ant` creates a XAR package.

I provide builds via the
[DARIAH-DE exist-repo](https://ci.de.dariah.eu/exist-repo/index.html).

## Installation
Use your preferd way of getting packages into eXist, e.g.
* put the `*.XAR` file in the folder `$EXIST_HOME/autodeploy`
* install it with the help of the dashboards package manager
* use the newer [package manager application](http://exist-db.org/exist/apps/public-repo/packages/packagemanager.html)
