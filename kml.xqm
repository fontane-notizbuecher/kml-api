xquery version "3.1";

module namespace f-kml="http://fontane-nb.dariah.eu/ns/SADE/kml";

import module namespace f-misc="http://fontane-nb.dariah.eu/ns/SADE/misc" at "/db/apps/SADE/modules/fontane/misc.xqm";
import module namespace restxq="http://exist-db.org/xquery/restxq" at "/db/apps/textgrid-connect/modules/restxq.xql";

declare namespace cache="https://sade.textgrid.de/ns/cache";
declare namespace geo="http://www.opengis.net/ont/geosparql#";
declare namespace gndo="http://d-nb.info/standards/elementset/gnd#";
declare namespace kml="http://www.opengis.net/kml/2.2";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare option exist:serialize "method=xhtml media-type=application/vnd.google-earth.kml+xml highlight-matches=none";

declare variable $f-kml:metadata-collection :=  collection("/db/sade-projects/textgrid/data/xml/meta");

declare
    %test:arg("id","4001409-5") %test:assertEquals("+012.436944","+050.987500")
    %test:arg("id","4000533-1") %test:assertEquals("")
function local:get-coordinates-from-GND($id as xs:string) as xs:string+ {
    let $point := local:cache-lookup("GND", $id)[last()]//text()
    (:  gives something like "Point ( +013.999240 +053.014989 )" :)
    let $x := $point => substring-after("Point ( ") => substring-before(" ")
    let $y :=  ($point => substring-before(" )") => tokenize(" "))[last()]
    return
        ($x, $y)
};

declare function local:get-coordinates-from-OSM($type as xs:string, $id as xs:string) {
    let $point := local:cache-lookup("OpenStreetMap:"||$type, $id)[last()]//text()
    return
        ( string($point/@lat), string($point/@lon) )
};

declare function local:cache-lookup($type as xs:string, $id as xs:string) {
    let $collection-uri := "/db/sade-projects/textgrid/cache/"
    let $resource-name := "kml.xml"
    let $document-uri := $collection-uri || $resource-name
    return
        if(doc-available($document-uri)) then
            let $item := doc($document-uri)//cache:item[@type=$type][@id=$id]
            return
                if (exists($item)) then $item/*
                else
                    local:cache($type, $id)
        else
            let $item := local:cache($type, $id)
            let $store :=   try {xmldb:store($collection-uri, $resource-name, <cache:items>{$item}</cache:items>)}
                            catch * {()}
            return
                $item
};

declare function local:cache($type as xs:string, $id as xs:string) {
    let $item :=
        switch ($type)
            case "GND" return
                let $request := httpclient:get(xs:anyURI("https://d-nb.info/gnd/"||$id||"/about/rdf"), false(), ())//httpclient:body/*
                let $location-node := $request//geo:asWKT
                return
                    if($location-node = () and $request//gndo:place) then
                        httpclient:get(xs:anyURI($request//gndo:place/string(@rdf:resource)||"/about/rdf"), false(), ())//httpclient:body//geo:asWKT
                    else $location-node
            case "OpenStreetMap:way" return
                let $first-node-of-the-way :=
                    string(httpclient:get(xs:anyURI("http://www.openstreetmap.org/api/0.6/way/"||$id), false(), ())//httpclient:body//nd[1]/@ref)
                return
                    local:cache-lookup("OpenStreetMap:node", $first-node-of-the-way)
            case "OpenStreetMap:node" return
                httpclient:get(xs:anyURI("http://www.openstreetmap.org/api/0.6/node/"||$id), false(), ())//httpclient:body//*:node
            default return <cache:item timestamp="{current-dateTime()}" type="{$type}" id="{$id}"/>
    let $store-in-cache :=  try { update insert
                                    <cache:item timestamp="{current-dateTime()}" type="{$type}" id="{$id}">
                                        {$item}
                                    </cache:item>
                                into doc("/db/sade-projects/textgrid/data/xml/cache.xml")/items }
                            catch * {()}
    return
        $item
};

declare
    %test:arg("notebook", "A01") %test:assertXPath("local-name($result) = 'kml'")
    %test:arg("notebook", "A\d\d") %test:assertEquals(120)
function f-kml:kml($notebook as xs:string) {

let $notebook-baseuri as xs:string+ := $f-kml:metadata-collection
            //tgmd:title[contains(., "Notizbuch")][matches(., $notebook)]
            /ancestor::tgmd:object//tgmd:textgridUri/substring-before(., ".")

return if($notebook-baseuri = "") then <kml xmlns="http://www.opengis.net/kml/2.2"> <!-- no documents found matching your query --></kml> else

let $register-root as element(tei:TEI) := doc("/db/sade-projects/textgrid/data/xml/data/253t2.xml")//tei:TEI

let $places := $register-root//tei:place[(tei:linkGrp/tei:link/contains(@target, $notebook-baseuri)) = true() ]

let $placemarks :=
    for $place in $places[tei:idno != ""]
    let $LODtype := string($place/tei:idno/@type)
    let $coordinates :=
        switch ($LODtype)
            case "GND" return $place/tei:idno[@type="GND"]/string() ! local:get-coordinates-from-GND(.)
            case "gnd" return $place/tei:idno[@type="gnd"]/string() ! local:get-coordinates-from-GND(.)
            case "OpenStreetMap" return $place/tei:idno[@type="OpenStreetMap"] ! local:get-coordinates-from-OSM( tokenize(./@xml:base, "/")[.!=""][last()], ./string() )
            default return "unsupported type: " || $LODtype
    let $title := string-join(($place/tei:placeName[not((@type, @resp))]), ", ")
    return
        <Placemark xmlns="http://www.opengis.net/kml/2.2">
            <address>{string($place/@xml:id)}</address>
            <description>{serialize(
                <div xmlns="http://www.w3.org/1999/xhtml">
                    <h1>{$title}</h1>
                    <ul style="color:lightgrey;list-style:none;">{for $variant in $place/tei:placeName[@type="variant"] return <li title="variant">{string($variant)}</li>}</ul>,
                    <ul class="lod">
                        {for $idno in $place/tei:idno
                         return
                            <li>
                                <a href="{string($idno/@xml:base)||string($idno)}">{string($idno/@type)||":"||string($idno)}</a>
                            </li>
                        }
                    </ul>
                    <ul class="notebooks">
                        {for $link in $place//tei:link
                        let $notebook-baseUri := string($link/@target) => substring-after("http://textgridrep.org/") => substring-before("#xpath(")
                        let $notebook-leaf := string($link/@target) => substring-after("surface[@n='") => substring-before("']//")
                        let $notebook-name := f-misc:get-title-by-baseUri( $notebook-baseUri )
                        where $notebook-name ne ""
                        return
                            <li><a href="https://fontane-nb.dariah.eu/test/edition.html?id=/xml/data/{$notebook-baseUri => substring-after(":")}.xml&amp;page={$notebook-leaf}" target="_blank">{$notebook-name || ": " || $notebook-leaf}</a></li>
                        }
                    </ul>
                </div>)}
            </description>
            <name>{$title}</name>
            {if(not($LODtype)) then () else
            <Point>
                <coordinates>{string-join($coordinates, ", ")}</coordinates>
            </Point>}
            <TimeStamp>
                <when></when>
            </TimeStamp>
        </Placemark>

return
<kml xmlns="http://www.opengis.net/kml/2.2">
    {$placemarks}
</kml>
};
