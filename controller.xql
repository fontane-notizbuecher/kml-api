xquery version "3.1";

import module namespace f-kml="http://fontane-nb.dariah.eu/ns/SADE/kml" at "kml.xqm";
import module namespace restxq="http://exist-db.org/xquery/restxq" at "restxq.xqm";
import module namespace console="http://exist-db.org/xquery/console";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

if ($exist:path = "/") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>

else if (matches($exist:path, ("/.+/fontane.kml"))) then
        let $notebooks := replace(tokenize($exist:path, "/")[last() - 1], "\\d", "[0-9]") => xmldb:decode()
        return
            f-kml:kml($notebooks)

else
(: everything is passed through :)
<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
    <cache-control cache="yes"/>
</dispatch>
